from datetime import datetime
from importlib import resources

import pytest

from gt_sat_api import DTE, AnulacionDTE, Direccion, Emisor, Frase, Item, Receptor

from . import examples


@pytest.fixture
def adte():
    """Creates basic AnulacionDTE"""
    emisor = Emisor(
        afiliacion_iva="GEN",
        codigo_establecimiento=1,
        correo_emisor="demo@demo.com.gt",
        nit_emisor="1000000000K",
        nombre_comercial="DEMO",
        nombre_emisor="DEMO, SOCIEDAD ANONIMA",
        direccion=Direccion(
            direccion="CUIDAD",
            codigo_postal="01001",
            municipio="GUATEMALA",
            departamento="GUATEMALA",
            pais="GT",
        ),
    )
    receptor = Receptor(
        correo_receptor="leyoalvizures4456@gmail.com",
        id_receptor="76365204",
        nombre_receptor="Jaime Alvizures",
        direccion=Direccion(
            direccion="CUIDAD",
            codigo_postal="01001",
            municipio="GUATEMALA",
            departamento="GUATEMALA",
            pais="GT",
        ),
    )
    return AnulacionDTE(
        uuid="BC779C8E-65EE-43F4-AB6E-B0A331CAE824",
        emisor=emisor,
        receptor=receptor,
        fecha_hora_emision=datetime.fromisoformat("2020-04-21T00:00:00-06:00"),
        fecha_hora_anulacion=datetime.fromisoformat("2020-04-21T00:00:00-06:00"),
        motivo_anulacion="PRUEBA DE ANULACIÓN",
    )


@pytest.fixture
def dte():
    """Creates basic DTE"""
    emisor = Emisor(
        afiliacion_iva="GEN",
        codigo_establecimiento=1,
        correo_emisor="demo@demo.com.gt",
        nit_emisor="1000000000K",
        nombre_comercial="DEMO",
        nombre_emisor="DEMO, SOCIEDAD ANONIMA",
        direccion=Direccion(
            direccion="CUIDAD",
            codigo_postal="01001",
            municipio="GUATEMALA",
            departamento="GUATEMALA",
            pais="GT",
        ),
    )
    receptor = Receptor(
        correo_receptor="leyoalvizures4456@gmail.com",
        id_receptor="76365204",
        nombre_receptor="Jaime Alvizures",
        direccion=Direccion(
            direccion="CUIDAD",
            codigo_postal="01001",
            municipio="GUATEMALA",
            departamento="GUATEMALA",
            pais="GT",
        ),
    )
    items = [
        Item(
            bien_o_servicio="B",
            numero_linea="1",
            cantidad=1.00,
            unidad_medida="UND",
            descripcion="PRODUCTO1",
            precio_unitario=100,
            descuento_porcentual=10.00,
            impuestos_rate={"IVA": (1, 100)},
        ),
    ]
    return DTE(
        clase_documento="dte",
        codigo_moneda="GTQ",
        fecha_hora_emision=datetime.fromisoformat("2020-04-21T09:58:00-06:00"),
        tipo="FACT",
        emisor=emisor,
        receptor=receptor,
        frases=[
            Frase(codigo_escenario=1, tipo_frase=1),
            Frase(codigo_escenario=1, tipo_frase=2),
        ],
        items=items,
    )


@pytest.fixture
def dte_peq_contr(dte):  # pylint: disable=redefined-outer-name
    """Creates DTE using Pequeño Contribyente"""
    dte.tipo = "FPEQ"
    return dte

@pytest.fixture
def dte_fcam(dte):  # pylint: disable=redefined-outer-name
    """Creates DTE using Factura Cambiaria"""
    dte.tipo = "FCAM"
    return dte

@pytest.fixture
def xml_factura_cambiaria_example():
    """Get XML content of examples/FacturaCambiaria.xml"""
    return resources.read_text(examples, "FacturaCambiaria.xml")

@pytest.fixture
def xml_peq_contr_example():
    """Get XML content of examples/Factura.xml"""
    return resources.read_text(examples, "FacturaPeqContr.xml")


@pytest.fixture
def xml_example():
    """Get XML content of examples/Factura.xml"""
    return resources.read_text(examples, "Factura.xml")


@pytest.fixture
def xml_annulled_example():
    """Get XML content of examples/Anulado.xml"""
    return resources.read_text(examples, "Anulado.xml")
