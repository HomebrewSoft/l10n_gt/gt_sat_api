from gt_sat_api.parsers import dte_to_xml_annulled


def test_dte2xml_annulled(adte, xml_annulled_example):
    """Test XML generated matched the examples"""
    xml_generated = dte_to_xml_annulled(adte)
    assert xml_generated == xml_annulled_example
