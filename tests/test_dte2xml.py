from gt_sat_api.parsers import dte_to_xml


def test_dte2xml(dte, xml_example):
    """Test XML generated matched the examples"""
    xml_generated = dte_to_xml(dte)
    assert xml_generated == xml_example


def test_dte_peq_contr_2xml(dte_peq_contr, xml_peq_contr_example):
    """Test XML generated matched the examples"""
    xml_generated = dte_to_xml(dte_peq_contr)
    assert xml_generated == xml_peq_contr_example

def test_dte_peq_contr_2xml(dte_fcam, xml_factura_cambiaria_example):
    """Test XML generated matched the examples"""
    xml_generated = dte_to_xml(dte_fcam)
    assert xml_generated == xml_factura_cambiaria_example
    
